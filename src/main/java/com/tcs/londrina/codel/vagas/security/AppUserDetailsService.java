package com.tcs.londrina.codel.vagas.security;

import com.tcs.londrina.codel.vagas.model.Users;
import com.tcs.londrina.codel.vagas.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class AppUserDetailsService implements UserDetailsService {

    @Autowired
    private LoginRepository loginRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Users> loginOptional = loginRepository.findByEmail(email);
        Users users = loginOptional.orElseThrow(() -> new UsernameNotFoundException("E-mail e/ou senha incorretos"));

        return new UserSystem(users, getPermissions(users));
    }

    private Collection<? extends GrantedAuthority> getPermissions(Users users) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        users.getPermissions().forEach(p -> authorities.add(new SimpleGrantedAuthority(p.getDescription().toUpperCase())));

        return authorities;
    }

}
