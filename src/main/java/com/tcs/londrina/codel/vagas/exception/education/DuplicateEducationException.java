package com.tcs.londrina.codel.vagas.exception.education;

public class DuplicateEducationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DuplicateEducationException(String message) {
        super(message);
    }

    public DuplicateEducationException(Long id) {
        this(String.format("Educação já cadastrada com o id %d", id));
    }

}