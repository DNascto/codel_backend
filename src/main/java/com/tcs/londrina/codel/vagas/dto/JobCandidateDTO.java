package com.tcs.londrina.codel.vagas.dto;

import com.tcs.londrina.codel.vagas.model.Job;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class JobCandidateDTO {

    private Long id;

    private String title;

    private String resume;

    private String requirements;

    private BigDecimal salary;

    private Integer candidates;

    private String nameCompany;

    private Boolean status;

    public JobCandidateDTO toDto(Job job) {
        this.id = job.getId();
        this.candidates = job.getCandidatesList().size();
        this.title = job.getTitle() ;
        this.requirements = job.getRequirements();
        this.salary = job.getSalary();
        this.status = job.getStatus();
        this.resume = job.getResume();
        this.nameCompany = job.getCompany().getFantasyName();
        return this;
    }

}
