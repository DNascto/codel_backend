package com.tcs.londrina.codel.vagas.service;

import com.tcs.londrina.codel.vagas.exception.competence.CompetenceInUseException;
import com.tcs.londrina.codel.vagas.exception.competence.CompetenceNotFoundException;
import com.tcs.londrina.codel.vagas.exception.competence.DuplicateCompetenceException;
import com.tcs.londrina.codel.vagas.model.Competence;
import com.tcs.londrina.codel.vagas.repository.CompetenceRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class CompetenceService {

    @Autowired
    private CompetenceRepository competenceRepository;

    public Competence findById(Long id) {
        return competenceRepository.findById(id).orElseThrow(() -> new CompetenceNotFoundException(id));
    }

    public Competence create(Competence competence) {
        Competence newCompetence = competenceRepository.findByName(competence.getName());

        if (newCompetence == null) {
            return competenceRepository.save(competence);
        } else {
            throw new DuplicateCompetenceException(newCompetence.getId());
        }
    }

    public Competence update(Long id, Competence competence) {
        Competence competenceFound = findById(id);

        BeanUtils.copyProperties(competence, competenceFound, "id");

        return competenceRepository.save(competenceFound);
    }

    public void deleteById(Long id) {
        try {
            competenceRepository.deleteById(id);

        } catch (EmptyResultDataAccessException e) {
            throw new CompetenceNotFoundException(id);

        } catch (DataIntegrityViolationException e) {
            throw new CompetenceInUseException(id);
        }
    }

}
