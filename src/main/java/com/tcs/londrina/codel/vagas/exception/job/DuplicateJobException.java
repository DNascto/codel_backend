package com.tcs.londrina.codel.vagas.exception.job;

public class DuplicateJobException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DuplicateJobException(String message) {
        super(message);
    }

    public DuplicateJobException(Long id) {
        this(String.format("Vaga já cadastrada com o id %d", id));
    }

}