package com.tcs.londrina.codel.vagas.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "job")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    private String title;

    @NotNull
    @Column(length = 100)
    private String resume;

    @NotNull
    @Column(length = 10000)
    private String requirements;

    @Column(length = 3000)
    private String differential;

    @NotNull
    private String jobType;

    private String regime;

    private BigDecimal salary;
    
    @NotNull
    private Boolean pcd = false;

    @NotNull
    private Boolean status;

    @CreationTimestamp
    @Column(name = "register_date")
    private LocalDate registerDate;

    @UpdateTimestamp
    @Column(name = "update_date")
    private LocalDate updateDate;
    
    @ManyToOne
    @JoinColumn(name = "id_position")
    @JsonProperty("id_position")
    private Position position;

    @ManyToOne
    @JoinColumn(name = "id_company", nullable = false)
    @JsonProperty("id_company")
    private Company company;

    @OneToMany(mappedBy = "job", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<CandidateJob> candidatesList;

}
