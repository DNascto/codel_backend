package com.tcs.londrina.codel.vagas.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.tcs.londrina.codel.vagas.exception.company.CompanyNotFoundException;
import com.tcs.londrina.codel.vagas.exception.company.DuplicateCompanyException;
import com.tcs.londrina.codel.vagas.exception.competence.CompetenceInUseException;
import com.tcs.londrina.codel.vagas.model.Company;
import com.tcs.londrina.codel.vagas.repository.CompanyRepository;
import com.tcs.londrina.codel.vagas.util.FilterPageable;
import com.tcs.londrina.codel.vagas.util.ValidationEmail;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;
    
    @Autowired(required = false)
    private BCryptPasswordEncoder encoder;

    @Autowired
    private ValidationEmail validationEmail;

    public Company findById(Long id) {
        return companyRepository.findById(id).orElseThrow(() -> new CompanyNotFoundException(id));
    }

    public Page<Company> findByPage(FilterPageable filterPageable) {
        return companyRepository.findAll(filterPageable.listByPage());
    }

    public Company create(Company company) {
        Company newCompany = companyRepository.findByCnpjAndEmail(company.getCnpj(),
                                                                  company.getUserCompany().getEmail());

        if (newCompany == null) {
        	validationEmail.isValidEmailAddress(company.getUserCompany().getEmail());
            company.getUserCompany().setPassword(encoder.encode(company.getUserCompany().getPassword()));
            
            return companyRepository.save(company);
        } else {
            throw new DuplicateCompanyException(newCompany.getId());
        }
    }

    public Company update(Long id, Company company) {
        Company companyFound = findById(id);

        BeanUtils.copyProperties(company, companyFound, "id", "jobsList");

        return companyRepository.save(companyFound);
    }

    public void deleteById(Long id) {
        try {
            companyRepository.deleteById(id);

        } catch (EmptyResultDataAccessException e) {
            throw new CompanyNotFoundException(id);

        } catch (DataIntegrityViolationException e) {
            throw new CompetenceInUseException(id);
        }
    }

}
