package com.tcs.londrina.codel.vagas.dto;

import com.tcs.londrina.codel.vagas.model.Job;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class JobDTO {

    private Long id;

    private String title;

    private String resume;

    private String requirements;

    private String differential;

    private String jobType;

    private String regime;

    private BigDecimal salary;

    private Boolean pcd = false;

    private Boolean status;

    private Long position;

    private Long idCompany;

    private String nameCompany;

    private LocalDate registerDate;

    private List<Long> candidates;

    public JobDTO toDto(Job job) {
        BeanUtils.copyProperties(job, this, "candidates");
        this.idCompany = job.getCompany().getId();
        this.nameCompany = job.getCompany().getFantasyName();
        this.candidates = new ArrayList<>();
        this.position = job.getPosition() != null ? job.getPosition().getId() : null;
        job.getCandidatesList().forEach(candidates -> this.candidates.add(candidates.getCandidate().getId()));
        return this;
    }

}
