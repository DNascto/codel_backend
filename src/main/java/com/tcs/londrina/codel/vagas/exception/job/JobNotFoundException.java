package com.tcs.londrina.codel.vagas.exception.job;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class JobNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public JobNotFoundException(String message) {
        super(message);
    }

  public JobNotFoundException(Long id) {
        this(String.format("Não existe uma vaga com o id %d", id));
    }

}