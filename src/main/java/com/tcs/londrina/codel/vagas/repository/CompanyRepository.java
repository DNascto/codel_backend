package com.tcs.londrina.codel.vagas.repository;

import com.tcs.londrina.codel.vagas.model.Candidate;
import com.tcs.londrina.codel.vagas.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM company AS c INNER JOIN user AS u ON c.user_id = u.id " +
            "WHERE c.cnpj = :cnpj OR u.email = :email")
    Company findByCnpjAndEmail(String cnpj, String email);

}
