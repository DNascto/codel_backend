package com.tcs.londrina.codel.vagas.exception.position;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PositionNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PositionNotFoundException(String message) {
        super(message);
    }

  public PositionNotFoundException(Long id) {
        this(String.format("Não existe um cargo com o id %d", id));
    }

}