package com.tcs.londrina.codel.vagas.repository;

import com.tcs.londrina.codel.vagas.model.Experience;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExperienceRepository extends JpaRepository<Experience, Long> {
}
