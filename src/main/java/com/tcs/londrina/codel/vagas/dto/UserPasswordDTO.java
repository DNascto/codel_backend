package com.tcs.londrina.codel.vagas.dto;

import lombok.Data;

@Data
public class UserPasswordDTO {

    private Long id;

    private String passwordOld;

    private String passwordNew;

}
