package com.tcs.londrina.codel.vagas.service;

import com.tcs.londrina.codel.vagas.dto.UserPasswordDTO;
import com.tcs.londrina.codel.vagas.exception.candidate.CandidateNotFoundException;
import com.tcs.londrina.codel.vagas.exception.company.CompanyNotFoundException;
import com.tcs.londrina.codel.vagas.exception.user.UserNotFoundException;
import com.tcs.londrina.codel.vagas.exception.user.UserPasswordException;
import com.tcs.londrina.codel.vagas.mail.Mailer;
import com.tcs.londrina.codel.vagas.model.Candidate;
import com.tcs.londrina.codel.vagas.model.Company;
import com.tcs.londrina.codel.vagas.model.Job;
import com.tcs.londrina.codel.vagas.model.Users;
import com.tcs.londrina.codel.vagas.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private Mailer mailer;

    public Users updatePasswordCandidate(UserPasswordDTO userPasswordDTO) {
        Users userFound = userRepository.findByUserCandidate(userPasswordDTO.getId());

        Users userSave = new Users();

        if (userFound != null) {
            if (encoder.matches(userPasswordDTO.getPasswordOld(), userFound.getPassword())) {
                userFound.setPassword(encoder.encode(userPasswordDTO.getPasswordNew()));
                userSave = userRepository.save(userFound);
            } else {
                throw new UserPasswordException("A senha atual está incorreta");
            }
            return userSave;
        } else {
            throw new CandidateNotFoundException(userPasswordDTO.getId());
        }
    }

    public Users updatePasswordCompany(UserPasswordDTO userPasswordDTO) {
        Users userFound = userRepository.findByUserCompany(userPasswordDTO.getId());

        Users userSave = new Users();

        if (userFound != null) {
            if (encoder.matches(userPasswordDTO.getPasswordOld(), userFound.getPassword())) {
                userFound.setPassword(encoder.encode(userPasswordDTO.getPasswordNew()));
                userSave = userRepository.save(userFound);
            } else {
                throw new UserPasswordException("A senha atual está incorreta");
            }
            return userSave;
        } else {
            throw new CompanyNotFoundException(userPasswordDTO.getId());
        }
    }

    public Users forgotPassword(Users users) {
        Users userFound = userRepository.findByEmail(users.getEmail());

        if (userFound != null) {
            userFound.setPassword("");
            String passwordRandom = randomGenerator();
            sendPassword(passwordRandom, userFound.getEmail());
            userFound.setPassword(encoder.encode(passwordRandom));

            return userRepository.save(userFound);
        } else {
            throw new UserNotFoundException("Usuário não encontrado");
        }
    }

    private String randomGenerator() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return generatedString;
    }

    public void sendPassword(String password, String email) {
        mailer.warnAboutPassword(password, email);
    }

}
