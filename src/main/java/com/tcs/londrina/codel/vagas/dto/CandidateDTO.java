package com.tcs.londrina.codel.vagas.dto;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.BeanUtils;

import com.tcs.londrina.codel.vagas.model.Address;
import com.tcs.londrina.codel.vagas.model.Candidate;
import com.tcs.londrina.codel.vagas.model.Competence;
import com.tcs.londrina.codel.vagas.model.Education;
import com.tcs.londrina.codel.vagas.model.Experience;
import com.tcs.londrina.codel.vagas.model.Gender;
import com.tcs.londrina.codel.vagas.model.Users;

import lombok.Data;

@Data
public class CandidateDTO {
	
	private Long id;

    private String name;

    private Gender gender;

    private LocalDate birthDate;

    private String cpf;

    private Address address;

    private Boolean status;

    private String phoneNumber;

    private String mobileNumber;

    private Boolean pcd;

    private String descriptionPcd;

    private String cnh;

    private Users userCandidate;

    private List<Education> educationList;

    private List<Experience> experienceList;

    private List<Competence> competences;
    
    private Set<Long> jobs;
        
    public CandidateDTO toDto(Candidate candidate) {
        BeanUtils.copyProperties(candidate, this, "jobs");
        this.jobs = new HashSet<>();
        candidate.getJobsList().forEach(jobs -> this.jobs.add(jobs.getJob().getId()));
        return this;
    }
    
}
