package com.tcs.londrina.codel.vagas.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "education")
@JsonIgnoreProperties({ "id_candidate" })
public class Education {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    private LocalDate startDate;

    private LocalDate endDate;

    @Enumerated(EnumType.ORDINAL)
    private Formation formation;

    private String institution;

    private String course;

    private String formationSituation;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_candidate")
    @JsonProperty("id_candidate")
    public Candidate candidate;

}
