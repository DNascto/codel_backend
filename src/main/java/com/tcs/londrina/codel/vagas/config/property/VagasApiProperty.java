package com.tcs.londrina.codel.vagas.config.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("codelvagas")
public class VagasApiProperty {

    private String allowedOrigin = "http://codel01.sercomtel.com.br";

    private final Security security = new Security();

    private final Mail mail = new Mail();

    // Getters
    public String getAllowedOrigin() {
        return allowedOrigin;
    }

    public Security getSecurity() {
        return security;
    }

    public Mail getMail() {
        return mail;
    }

    public static class Security {
        private boolean enableHttps;

        public boolean isEnableHttps() {
            return enableHttps;
        }

        public void setEnableHttps(boolean enableHttps) {
            this.enableHttps = enableHttps;
        }
    }

    public static class Mail {

        private String host;
        private Integer port;
        private String username;
        private String password;

        // Getters e Setters
        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public Integer getPort() {
            return port;
        }

        public void setPort(Integer port) {
            this.port = port;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

}
