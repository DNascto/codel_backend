package com.tcs.londrina.codel.vagas.repository;

import com.tcs.londrina.codel.vagas.model.Candidate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM candidate AS c INNER JOIN user AS u ON c.user_id = u.id " +
                                       "WHERE c.cpf = :cpf OR u.email = :email")
    Candidate findByCpfAndEmail(String cpf, String email);

}
