package com.tcs.londrina.codel.vagas.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.br.CNPJ;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "company")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    private String fantasyName;

    @NotNull
    @CNPJ
    private String cnpj;

    @Embedded
    private Address address;

    @NotNull
    private String nameOfResponsible;

    @NotNull
    private String officeNumber;

    @NotNull
    private String responsibleNumber;

    @NotNull
    private String activityBranch;
    
    @CreationTimestamp
    @Column(name = "register_date")
    private LocalDate registerDate;

    @UpdateTimestamp
    @Column(name = "update_date")
    private LocalDate updateDate;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private Users userCompany;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_company")
    @JsonIgnoreProperties({ "id_company" })
    private List<Job> jobsList;

}
