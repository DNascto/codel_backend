package com.tcs.londrina.codel.vagas.controller;

import com.tcs.londrina.codel.vagas.model.Education;
import com.tcs.londrina.codel.vagas.repository.EducationRepository;
import com.tcs.londrina.codel.vagas.service.EducationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/education")
public class EducationController {

    @Autowired
    private EducationRepository educationRepository;

    @Autowired
    private EducationService educationService;

    @GetMapping
    public List<Education> findAll() {
        return educationRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Education> findById(@PathVariable Long id) {
        Education educationFound = educationService.findById(id);

        return educationFound != null ? ResponseEntity.ok(educationFound) : ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<Education> create(@Valid @RequestBody Education education) {
        Education newEducation = educationService.create(education);

        return ResponseEntity.status(HttpStatus.CREATED).body(newEducation);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Education> update(@PathVariable Long id, @Valid @RequestBody Education education) {
        Education educationFound = educationService.update(id, education);

        return ResponseEntity.ok(educationFound);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        educationService.deleteById(id);
    }

}
