package com.tcs.londrina.codel.vagas.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Gender {

    M("Masculino"),
    F("Feminino"),
    O("Outros");

    private String description;

}
