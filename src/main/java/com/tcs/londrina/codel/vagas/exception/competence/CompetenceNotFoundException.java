package com.tcs.londrina.codel.vagas.exception.competence;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CompetenceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CompetenceNotFoundException(String message) {
        super(message);
    }

  public CompetenceNotFoundException(Long id) {
        this(String.format("Não existe uma competência com o id %d", id));
    }

}