package com.tcs.londrina.codel.vagas.controller;

import com.tcs.londrina.codel.vagas.dto.CompanyDTO;
import com.tcs.londrina.codel.vagas.dto.CompanyJobDTO;
import com.tcs.londrina.codel.vagas.dto.JobCandidateDTO;
import com.tcs.londrina.codel.vagas.dto.UserPasswordDTO;
import com.tcs.londrina.codel.vagas.model.Company;
import com.tcs.londrina.codel.vagas.model.Users;
import com.tcs.londrina.codel.vagas.repository.CompanyRepository;
import com.tcs.londrina.codel.vagas.service.CompanyService;
import com.tcs.londrina.codel.vagas.service.UserService;
import com.tcs.londrina.codel.vagas.util.FilterPageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private UserService userService;

    @GetMapping("/all")
    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CompanyDTO> findById(@PathVariable Long id) {
        Company companyFound = companyService.findById(id);
        CompanyDTO dto = new CompanyDTO().toDto(companyFound);

        return companyFound != null ? ResponseEntity.ok(dto) : ResponseEntity.notFound().build();
    }

    @GetMapping("/page")
    public Page<Company> findByPage(FilterPageable filterPageable) {
        return companyService.findByPage(filterPageable);
    }

    @GetMapping("/job/{id}")
    public  ResponseEntity<CompanyJobDTO> findCompanyJob(@PathVariable Long id) {
        Company companyFound = companyService.findById(id);
        CompanyJobDTO dto = new CompanyJobDTO().toDto(companyFound);

        return companyFound != null ? ResponseEntity.ok(dto) : ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<Company> create(@Valid @RequestBody Company company) {
        Company newCompany = companyService.create(company);

        return ResponseEntity.status(HttpStatus.CREATED).body(newCompany);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Company> update(@PathVariable Long id, @RequestBody Company company) {
        Company companyFound = companyService.update(id, company);

        return ResponseEntity.ok(companyFound);
    }

    @PutMapping("/password")
    public ResponseEntity<Void> updatePassword(@Valid @RequestBody UserPasswordDTO userPasswordDTO) {
        Users userFound = userService.updatePasswordCompany(userPasswordDTO);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        companyService.deleteById(id);
    }

}
