package com.tcs.londrina.codel.vagas.exception.user;

public class UserPasswordException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UserPasswordException(String message) {
        super(message);
    }

}
