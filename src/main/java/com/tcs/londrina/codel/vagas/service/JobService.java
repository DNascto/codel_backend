package com.tcs.londrina.codel.vagas.service;

import com.tcs.londrina.codel.vagas.exception.util.NumberOfCharactersException;
import com.tcs.londrina.codel.vagas.model.Company;
import com.tcs.londrina.codel.vagas.model.Position;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.tcs.londrina.codel.vagas.exception.job.JobInUseException;
import com.tcs.londrina.codel.vagas.exception.job.JobNotFoundException;
import com.tcs.londrina.codel.vagas.model.Job;
import com.tcs.londrina.codel.vagas.repository.JobRepository;
import com.tcs.londrina.codel.vagas.util.FilterPageable;

import java.util.List;

@Service
public class JobService {

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private CompanyService companyService;

    public Job findById(Long id) {
        return jobRepository.findById(id).orElseThrow(() -> new JobNotFoundException(id));
    }

    public Page<Job> findByPage(FilterPageable filterPageable) {
        return jobRepository.findByStatusTrue(filterPageable.listByPage());
    }

    public List<Job> searchByTitleAndRequirementsAndDifferential(String title) {
        return jobRepository.findJobByTitleAndRequirementsAndDifferential(title + "*");
    }

    public Job create(Job job) {
        Company company = companyService.findById(job.getCompany().getId());
        job.setCompany(company);

        return jobRepository.save(job);
    }

    public Job update(Long id, Job job) {
        Job jobFound = findById(id);

        BeanUtils.copyProperties(job, jobFound, "id", "candidates", "id_company");

        return jobRepository.save(jobFound);
    }

    public void deleteById(Long id) {
        try {
            jobRepository.deleteById(id);

        } catch (EmptyResultDataAccessException e) {
            throw new JobNotFoundException(id);

        } catch (DataIntegrityViolationException e) {
            throw new JobInUseException(id);
        }
    }

}
