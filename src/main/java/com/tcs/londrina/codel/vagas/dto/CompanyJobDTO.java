package com.tcs.londrina.codel.vagas.dto;

import com.tcs.londrina.codel.vagas.model.Company;
import com.tcs.londrina.codel.vagas.model.Job;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

@Data
public class CompanyJobDTO {

    private List<JobCandidateDTO> jobsList;

    public CompanyJobDTO toDto(Company company) {
        this.jobsList = new ArrayList<>();
        company.getJobsList().forEach(
                job -> {
                    JobCandidateDTO jobCandidateDTO = new JobCandidateDTO();
                    jobCandidateDTO.toDto(job);
                    this.jobsList.add(jobCandidateDTO);
                }
        );

        System.out.println(this.jobsList);

        return this;
    }

}
