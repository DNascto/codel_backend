package com.tcs.londrina.codel.vagas.exception.candidate;

public class DuplicateCandidateException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DuplicateCandidateException(String message) {
        super(message);
    }

    public DuplicateCandidateException(Long id) {
        this(String.format("Candidatdo como já cadastra o cpf ou email pelo id %d", id));
    }

}