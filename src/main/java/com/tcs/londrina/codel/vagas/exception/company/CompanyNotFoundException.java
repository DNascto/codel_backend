package com.tcs.londrina.codel.vagas.exception.company;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CompanyNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CompanyNotFoundException(String message) {
        super(message);
    }

  public CompanyNotFoundException(Long id) {
        this(String.format("Não existe uma empresa com o id %d", id));
    }

}