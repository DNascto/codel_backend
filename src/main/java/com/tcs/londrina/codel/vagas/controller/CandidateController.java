package com.tcs.londrina.codel.vagas.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.tcs.londrina.codel.vagas.dto.CandidateJobDTO;
import com.tcs.londrina.codel.vagas.dto.CandidateJobReadedDTO;
import com.tcs.londrina.codel.vagas.dto.UserPasswordDTO;
import com.tcs.londrina.codel.vagas.model.CandidateJob;
import com.tcs.londrina.codel.vagas.model.Users;
import com.tcs.londrina.codel.vagas.repository.CandidateJobRepository;
import com.tcs.londrina.codel.vagas.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.londrina.codel.vagas.dto.CandidateDTO;
import com.tcs.londrina.codel.vagas.model.Candidate;
import com.tcs.londrina.codel.vagas.repository.CandidateRepository;
import com.tcs.londrina.codel.vagas.service.CandidateService;
import com.tcs.londrina.codel.vagas.util.FilterPageable;

@RestController
@RequestMapping("/candidate")
public class CandidateController {

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private CandidateJobRepository candidateJobRepository;

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private UserService userService;

    @GetMapping("/all")
    public List<Candidate> findAll() {
        return candidateRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CandidateDTO> findById(@PathVariable Long id) {
        Candidate candidateFound = candidateService.findById(id);
        CandidateDTO dto = new CandidateDTO().toDto(candidateFound);
        
        return candidateFound != null ? ResponseEntity.ok(dto) : ResponseEntity.notFound().build();
    }

    @GetMapping("/page")
    public Page<Candidate> findByPage(FilterPageable filterPageable) {
        return candidateService.findByPage(filterPageable);
    }

    @GetMapping("/job/{id}")
    public  ResponseEntity<CandidateJobDTO> findCandidateJob(@PathVariable Long id) {
        Candidate candidateFound = candidateService.findById(id);
        CandidateJobDTO dto = new CandidateJobDTO().toDto(candidateFound);

        return candidateFound != null ? ResponseEntity.ok(dto) : ResponseEntity.notFound().build();
    }

    @GetMapping("/job/{id}/amount")
    public ResponseEntity<Integer> amountCandidateJob(@PathVariable Long id) {
        Integer result = candidateJobRepository.amouthCandidate(id);

        return ResponseEntity.ok(result);
    }

    @GetMapping("/readed/true/company/{id}")
    public List<CandidateJob> findByReadedTrue(@PathVariable Long id) {
        return candidateJobRepository.findByReadedTrue(id);
    }

    @GetMapping("/readed/false/company/{id}")
    public List<CandidateJob> findByReadedFalse(@PathVariable Long id) {
        return candidateJobRepository.findByReadedFalse(id);
    }

    @GetMapping("/readed/company/{id}")
    public List<CandidateJob> findByReaded(@PathVariable Long id) {
        return candidateJobRepository.findByReaded(id);
    }

    @GetMapping("/job/{id}/name")
    public List<CandidateJobReadedDTO> findByCandidateJobWithName(@PathVariable Long id) {
        List<CandidateJobReadedDTO> dto = candidateService.findByCandidateJobWithName(id);

        return dto;
    }

    @PostMapping
    public ResponseEntity<Candidate> create(@Valid @RequestBody Candidate candidate) {
        Candidate newCandidate = candidateService.create(candidate);

        return ResponseEntity.status(HttpStatus.CREATED).body(newCandidate);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Candidate> update(@PathVariable Long id, @Valid @RequestBody Candidate candidate) {
        Candidate candidateFound = candidateService.update(id, candidate);

        return ResponseEntity.ok(candidateFound);
    }

    @PutMapping("/{id}/job")
    public ResponseEntity<Boolean> updateJob(@PathVariable Long id, @Valid @RequestBody CandidateJob candidateJob) {
        Boolean candidateFound = candidateService.updateJob(id, candidateJob);

        return ResponseEntity.ok(candidateFound);
    }

    @PutMapping("/password")
    public ResponseEntity<Void> updatePassword(@Valid @RequestBody UserPasswordDTO userPasswordDTO) {
        Users userFound = userService.updatePasswordCandidate(userPasswordDTO);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        candidateService.deleteById(id);
    }

}
