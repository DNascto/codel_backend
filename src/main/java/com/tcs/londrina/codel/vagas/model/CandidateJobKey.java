package com.tcs.londrina.codel.vagas.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CandidateJobKey implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "id_candidate")
	private Long idCandidate;
	
	@Column(name = "id_job")
	private Long idJob;

	public CandidateJobKey() {

	}

	public CandidateJobKey(Long idCandidate, Long idJob) {
		this.idCandidate = idCandidate;
		this.idJob = idJob;
	}

	public Long getIdCandidate() {
		return idCandidate;
	}

	public void setIdCandidate(Long idCandidate) {
		this.idCandidate = idCandidate;
	}

	public Long getIdJob() {
		return idJob;
	}

	public void setIdJob(Long idJob) {
		this.idJob = idJob;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		CandidateJobKey that = (CandidateJobKey) o;
		return Objects.equals(idCandidate, that.idCandidate) &&
				Objects.equals(idJob, that.idJob);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idCandidate, idJob);
	}
}
