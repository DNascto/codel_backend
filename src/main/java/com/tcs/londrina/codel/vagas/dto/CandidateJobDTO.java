package com.tcs.londrina.codel.vagas.dto;

import com.tcs.londrina.codel.vagas.model.Candidate;
import com.tcs.londrina.codel.vagas.model.Company;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CandidateJobDTO {

    private List<JobCandidateDTO> jobsList;

    public CandidateJobDTO toDto(Candidate candidate) {
        this.jobsList = new ArrayList<>();
        candidate.getJobsList().forEach(
                jobs -> {
                    JobCandidateDTO jobCandidateDTO = new JobCandidateDTO();
                    jobCandidateDTO.toDto(jobs.getJob());
                    this.jobsList.add(jobCandidateDTO);
                }
        );

        return this;
    }

}
