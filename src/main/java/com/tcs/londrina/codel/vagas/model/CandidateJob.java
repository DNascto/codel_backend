package com.tcs.londrina.codel.vagas.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "candidate_job")
@Data
public class CandidateJob {

	@EmbeddedId
	private CandidateJobKey id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("id_candidate")
    @JoinColumn(name = "id_candidate")
    @JsonIgnore
    private Candidate candidate;
 
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("id_job")
    @JoinColumn(name = "id_job")
    @JsonIgnore
    private Job job;
 
    private Boolean sendEmail = false;

    private Integer score = 0;

    private Boolean readed = false;

    // Construtors
    public CandidateJob() {}

    public CandidateJob(Candidate candidate, Job job, Boolean readed, Integer score, Boolean sendEmail) {
        this.id = new CandidateJobKey(candidate.getId(), job.getId());
        this.candidate = candidate;
        this.job = job;
        this.readed = readed;
        this.score = score;
        this.sendEmail = sendEmail;
    }

    // Getters e Setters
    public CandidateJobKey getId() {
        return id;
    }

    public void setId(CandidateJobKey id) {
        this.id = id;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public boolean isSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isReaded() {
        return readed;
    }

    public void setReaded(boolean readed) {
        this.readed = readed;
    }

    // Equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        CandidateJob that = (CandidateJob) o;
        return Objects.equals(candidate, that.candidate) &&
                Objects.equals(job, that.job);
    }

    // HashCode
    @Override
    public int hashCode() {
        return Objects.hash(candidate, job);
    }
}
