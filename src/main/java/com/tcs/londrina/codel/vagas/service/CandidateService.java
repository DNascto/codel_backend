package com.tcs.londrina.codel.vagas.service;

import com.tcs.londrina.codel.vagas.dto.CandidateJobReadedDTO;
import com.tcs.londrina.codel.vagas.exception.candidate.CandidateInUseException;
import com.tcs.londrina.codel.vagas.exception.candidate.CandidateNotFoundException;
import com.tcs.londrina.codel.vagas.exception.candidate.DuplicateCandidateException;
import com.tcs.londrina.codel.vagas.mail.Mailer;
import com.tcs.londrina.codel.vagas.model.Candidate;
import com.tcs.londrina.codel.vagas.model.CandidateJob;
import com.tcs.londrina.codel.vagas.model.Company;
import com.tcs.londrina.codel.vagas.model.Job;
import com.tcs.londrina.codel.vagas.repository.CandidateJobRepository;
import com.tcs.londrina.codel.vagas.repository.CandidateRepository;
import com.tcs.londrina.codel.vagas.util.FilterPageable;
import com.tcs.londrina.codel.vagas.util.ValidationEmail;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CandidateService {

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private CandidateJobRepository candidateJobRepository;

    @Autowired
    private JobService jobService;

    @Autowired
    private CompanyService companyService;

    @Autowired(required = false)
    private BCryptPasswordEncoder encoder;

    @Autowired
    private ValidationEmail validationEmail;

    @Autowired
    private Mailer mailer;

    public Candidate findById(Long id) {
        return candidateRepository.findById(id).orElseThrow(() -> new CandidateNotFoundException(id));
    }

    public Page<Candidate> findByPage(FilterPageable filterPageable) {
        return candidateRepository.findAll(filterPageable.listByPage());
    }

    public List<CandidateJobReadedDTO> findByCandidateJobWithName(Long id) {
        List<CandidateJob> candidateJobList = candidateJobRepository.findByCandidateJobWithName(id);

        return convertToDTO(candidateJobList);
    }

    public Candidate create(Candidate candidate) {
        Candidate newCandidate = candidateRepository.findByCpfAndEmail(candidate.getCpf(),
                                                                       candidate.getUserCandidate().getEmail());

        if (newCandidate == null) {
            validationEmail.isValidEmailAddress(candidate.getUserCandidate().getEmail());
            candidate.getUserCandidate().setPassword(encoder.encode(candidate.getUserCandidate().getPassword()));

            return candidateRepository.save(candidate);
        } else {
            throw new DuplicateCandidateException("Candidatdo como já cadastra o cpf ou email");
        }
    }

    public Candidate update(Long id, Candidate candidate) {
        Candidate candidateFound = findById(id);

        BeanUtils.copyProperties(candidate, candidateFound, "id");

        return candidateRepository.save(candidateFound);
    }

    public Boolean updateJob(Long id, CandidateJob candidateJob) {
        Candidate candidateFound = findById(id);
        Job jobFound = jobService.findById(candidateJob.getId().getIdJob());
        Boolean readed = candidateJob.isReaded();
        Integer score = candidateJob.getScore();
        Boolean sendEmail = candidateJob.isSendEmail();
        CandidateJob cj = new CandidateJob(candidateFound, jobFound, readed, score, sendEmail);

        if (cj.getSendEmail() == false) {
            sendResumeToCompany(cj.getId().getIdCandidate(), cj.getJob().getCompany().getId(), cj.getId().getIdJob());
            cj.setSendEmail(true);
        }

        candidateJobRepository.save(cj);

        return true;
    }

    public void deleteById(Long id) {
        try {
            candidateRepository.deleteById(id);

        } catch (EmptyResultDataAccessException e) {
            throw new CandidateNotFoundException(id);

        } catch (DataIntegrityViolationException e) {
            throw new CandidateInUseException(id);
        }
    }

    private List<CandidateJobReadedDTO> convertToDTO(List<CandidateJob> candidateJobList) {
        List<CandidateJobReadedDTO> listaDTO = new ArrayList<>();

        for (CandidateJob candidateJob : candidateJobList){
            CandidateJobReadedDTO dto = new CandidateJobReadedDTO();
            listaDTO.add(dto.toDto(candidateJob));
        }

        return listaDTO;
    }

    public void sendResumeToCompany(Long idCandidate, Long idCompany, Long idJob) {
        Candidate candidate = findById(idCandidate);
        Company company = companyService.findById(idCompany);
        Job job = jobService.findById(idJob);

        mailer.warnAboutCandidateVacancies(candidate, company, job);
    }

}
