package com.tcs.londrina.codel.vagas.exception.job;

public class JobInUseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public JobInUseException(String message) {
        super(message);
    }

    public JobInUseException(Long id) {
        this(String.format("A vaga do id %d está em uso", id));
    }

}
