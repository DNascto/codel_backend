package com.tcs.londrina.codel.vagas.controller;

import java.util.List;

import javax.validation.Valid;

import com.tcs.londrina.codel.vagas.dto.JobDTO;
import com.tcs.londrina.codel.vagas.model.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.londrina.codel.vagas.model.Job;
import com.tcs.londrina.codel.vagas.repository.JobRepository;
import com.tcs.londrina.codel.vagas.service.JobService;
import com.tcs.londrina.codel.vagas.util.FilterPageable;

@RestController
@RequestMapping("/job")
public class JobController {

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private JobService jobService;

    @GetMapping
    public List<Job> findAll() {
        return jobRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<JobDTO> findById(@PathVariable Long id) {
        Job jobFound = jobService.findById(id);
        JobDTO dto = new JobDTO().toDto(jobFound);
        
        return jobFound != null ? ResponseEntity.ok(dto) : ResponseEntity.notFound().build();
    }

    @GetMapping("/search")
    public List<Job> searchByTitleAndRequirementsAndDifferential(@RequestParam String title) {
        return jobService.searchByTitleAndRequirementsAndDifferential(title);
    }

    @GetMapping("/status/true/page")
    public Page<Job> findByPage(FilterPageable filterPageable) {
        return jobService.findByPage(filterPageable);
    }

    @PostMapping
    public ResponseEntity<Job> create(@Valid @RequestBody Job job) {
        Job jobFound = jobService.create(job);

        return ResponseEntity.status(HttpStatus.CREATED).body(jobFound);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Job> update(@PathVariable Long id, @Valid @RequestBody Job job) {
        Job jobFound = jobService.update(id, job);

        return ResponseEntity.ok(jobFound);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        jobService.deleteById(id);
    }

}
