package com.tcs.londrina.codel.vagas.exception.candidate_job;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CandidateJobNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CandidateJobNotFoundException(String message) {
        super(message);
    }

}