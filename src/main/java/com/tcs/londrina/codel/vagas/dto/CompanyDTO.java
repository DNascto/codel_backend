package com.tcs.londrina.codel.vagas.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.tcs.londrina.codel.vagas.model.Address;
import com.tcs.londrina.codel.vagas.model.Company;
import com.tcs.londrina.codel.vagas.model.Users;

import lombok.Data;

@Data
public class CompanyDTO {
	
    private Long id;

    private String fantasyName;

    private String cnpj;

    private Address address;

    private String nameOfResponsible;

    private String officeNumber;

    private String responsibleNumber;

    private String activityBranch;

    private Users userCompany;

    private List<Long> jobs;
    
    public CompanyDTO toDto(Company company) {
    	BeanUtils.copyProperties(company, this, "jobs");
    	this.jobs = new ArrayList<>();
    	company.getJobsList().forEach(job -> this.jobs.add(job.getId()));
    	
    	return this;
    }

}
