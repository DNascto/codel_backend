package com.tcs.londrina.codel.vagas.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@Embeddable
public class Address {

    private String street;
    private String addressNumber;
    private String complement;
    private String district;
    private String city;
    private String state;

    @Column(name = "zip_code")
    private String zipCode;

}
