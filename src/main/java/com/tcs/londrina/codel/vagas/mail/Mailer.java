package com.tcs.londrina.codel.vagas.mail;

import com.tcs.londrina.codel.vagas.model.Candidate;
import com.tcs.londrina.codel.vagas.model.Company;
import com.tcs.londrina.codel.vagas.model.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
public class Mailer {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private TemplateEngine thymeleaf;

    public void warnAboutCandidateVacancies(Candidate candidate, Company receiver, Job job) {
        Map<String, Object> variable = new HashMap<>();
        variable.put("candidate", candidate);

        String email = receiver.getUserCompany().getEmail();
        String subject = job.getTitle();

        this.sendEmail("codelteste@gmail.com",
                email,
                "Currículo cadastrado na vaga " + subject ,
                "candidate-resume",
                variable);
    }

    public void warnAboutPassword(String password, String email) {
        Map<String, Object> variable = new HashMap<>();
        variable.put("password", password);

        String emailEnviado = email;

        this.sendEmail("codelteste@gmail.com",
                email,
                "Reset de senha",
                "reset-password",
                variable);
    }


    public void sendEmail(String sender, String receiver, String subject, String template,
                            Map<String, Object> variable) {
        Context context = new Context(new Locale("pt", "BR"));
        context.setVariable("birthDate", Calendar.getInstance());
        context.setVariable("startDate", Calendar.getInstance());
        context.setVariable("endDate", Calendar.getInstance());

        variable.entrySet().forEach(e -> context.setVariable(e.getKey(), e.getValue()));

        String message = thymeleaf.process(template, context);

        this.sendEmail(sender, receiver, subject, message);
    }

    public void sendEmail(String sender, String receiver, String subject, String message) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();

            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
            mimeMessageHelper.setFrom(sender);
            mimeMessageHelper.setTo(receiver);
            mimeMessageHelper.setSubject(subject);
            mimeMessageHelper.setText(message, true);

            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            throw new RuntimeException("Problemas com o envio de e-mail", e);
        }
    }

}
