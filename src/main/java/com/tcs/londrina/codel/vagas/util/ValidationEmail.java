package com.tcs.londrina.codel.vagas.util;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.tcs.londrina.codel.vagas.exception.email.EmailInvalidException;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ValidationEmail {

    public boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            throw new EmailInvalidException("E-mail Inválido!!");
        }
        return result;
    }

}

