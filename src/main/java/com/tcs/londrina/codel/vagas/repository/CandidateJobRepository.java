package com.tcs.londrina.codel.vagas.repository;

import com.tcs.londrina.codel.vagas.model.CandidateJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidateJobRepository extends JpaRepository<CandidateJob, Long> {

    @Query(nativeQuery = true, value = "SELECT COUNT(readed) FROM candidate_job CJ WHERE CJ.id_job = :idJob AND CJ.readed = TRUE")
    Integer amouthCandidate(@Param("idJob") Long id);

    @Query(nativeQuery = true, value = "SELECT jb.* FROM candidate_job jb, company c, job j " +
                                       "WHERE j.id_company = :idCompany AND jb.readed = true " +
                                                                       "AND jb.id_job = j.id")
    List<CandidateJob> findByReadedTrue(@Param("idCompany") Long idCompany);

    @Query(nativeQuery = true, value = "SELECT jb.* FROM candidate_job jb, company c, job j " +
                                       "WHERE j.id_company = :idCompany AND jb.readed = false " +
                                                                       "AND jb.id_job = j.id")
    List<CandidateJob> findByReadedFalse(@Param("idCompany") Long idCompany);

    @Query(nativeQuery = true, value = "SELECT jb.* FROM candidate_job jb, company c, job j " +
                                       "WHERE j.id_company = :idCompany AND jb.id_job = j.id")
    List<CandidateJob> findByReaded(@Param("idCompany") Long idCompany);

    @Query("SELECT cj from CandidateJob cj JOIN cj.candidate c WHERE cj.id.idJob = :idJob")
    List<CandidateJob> findByCandidateJobWithName(@Param("idJob") Long id);

}