package com.tcs.londrina.codel.vagas.exception.candidate;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CandidateNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CandidateNotFoundException(String message) {
        super(message);
    }

  public CandidateNotFoundException(Long id) {
        this(String.format("Não existe um candidato com o id %d", id));
    }

}