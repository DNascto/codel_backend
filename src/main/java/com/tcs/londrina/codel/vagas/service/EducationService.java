package com.tcs.londrina.codel.vagas.service;

import com.tcs.londrina.codel.vagas.exception.education.EducationInUseException;
import com.tcs.londrina.codel.vagas.exception.education.EducationNotFoundException;
import com.tcs.londrina.codel.vagas.model.Education;
import com.tcs.londrina.codel.vagas.repository.EducationRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class EducationService {

    @Autowired
    private EducationRepository educationRepository;

    public Education findById(Long id) {
        return educationRepository.findById(id).orElseThrow(() -> new EducationNotFoundException(id));
    }

    public Education create(Education education) {
        return educationRepository.save(education);
    }

    public Education update(Long id, Education education) {
        Education educationFound = findById(id);

        BeanUtils.copyProperties(education, educationFound, "id");

        return educationRepository.save(educationFound);
    }

    public void deleteById(Long id) {
        try {
            educationRepository.deleteById(id);

        } catch (EmptyResultDataAccessException e) {
            throw new EducationNotFoundException(id);

        } catch (DataIntegrityViolationException e) {
            throw new EducationInUseException(id);
        }
    }

}
