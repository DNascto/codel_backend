package com.tcs.londrina.codel.vagas.dto;

import com.tcs.londrina.codel.vagas.model.CandidateJob;
import com.tcs.londrina.codel.vagas.model.Job;
import lombok.Data;

@Data
public class CandidateJobReadedDTO {

    private Long id;
    private Long idCandidate;
    private String nameCandidate;
    private String nameJob;
    private Boolean readed;
    private Integer score;

    public CandidateJobReadedDTO toDto(CandidateJob candidateJob) {
        this.id = candidateJob.getId().getIdJob();
        this.idCandidate = candidateJob.getId().getIdCandidate();
        this.nameCandidate = candidateJob.getCandidate().getName();
        this.nameJob = candidateJob.getJob().getTitle();
        this.readed = candidateJob.isReaded();
        this.score = candidateJob.getScore();

        return this;
    }

}
