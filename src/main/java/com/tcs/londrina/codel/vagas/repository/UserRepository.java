package com.tcs.londrina.codel.vagas.repository;

import com.tcs.londrina.codel.vagas.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

    @Query(nativeQuery = true, value = "SELECT U.* FROM user U WHERE U.id IN \n" +
                                       "\t(SELECT C.user_id FROM candidate C WHERE C.id = :idCandidate);")
    Users findByUserCandidate(@Param("idCandidate") Long id);

    @Query(nativeQuery = true, value = "SELECT U.* FROM user U WHERE U.ID IN \n" +
            "\t(SELECT C.USER_ID FROM company C WHERE C.ID = :idCompany);")
    Users findByUserCompany(@Param("idCompany") Long id);

    @Query(nativeQuery = true, value = "SELECT * FROM user U WHERE U.EMAIL = :email")
    Users findByEmail(@Param("email") String email);

}
