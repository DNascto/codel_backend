package com.tcs.londrina.codel.vagas.controller;

import com.tcs.londrina.codel.vagas.model.Users;
import com.tcs.londrina.codel.vagas.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PatchMapping("/reset-password")
    public ResponseEntity<Users> forgotPassword(@RequestBody Users users) {
        Users userFound = userService.forgotPassword(users);

        return ResponseEntity.ok(userFound);
    }

}
