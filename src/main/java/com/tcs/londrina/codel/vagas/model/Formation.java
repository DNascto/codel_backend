package com.tcs.londrina.codel.vagas.model;

public enum Formation {

    ELEMENTARY ("Ensino Fundamental"),
    HIGH_SCHOOL ("Ensino Médio"),
    TECHNICAL_HIGH_SCHOOL ("Ensino Médio Técnico"),
    GRADUATE ("Graduação"),
    SPECIALIZATION ("Pós Graduação - Especialização"),
    CERTIFICATION ("Certificação"),
    MASTER ("Pós Graduação - Mestrado"),
    DOCTORATE ("Pós Graduação - Doutorado");

    private String description;

    private Formation(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
