package com.tcs.londrina.codel.vagas.exception.education;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EducationNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EducationNotFoundException(String message) {
        super(message);
    }

  public EducationNotFoundException(Long id) {
        this(String.format("Não existe uma educação com o id %d", id));
    }

}