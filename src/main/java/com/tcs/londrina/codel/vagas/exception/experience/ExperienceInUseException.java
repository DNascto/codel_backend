package com.tcs.londrina.codel.vagas.exception.experience;

public class ExperienceInUseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ExperienceInUseException(String message) {
        super(message);
    }

    public ExperienceInUseException(Long id) {
        this(String.format("A experiência do id %d está em uso", id));
    }

}
