package com.tcs.londrina.codel.vagas.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.londrina.codel.vagas.model.Position;
import com.tcs.londrina.codel.vagas.repository.PositionRepository;
import com.tcs.londrina.codel.vagas.service.PositionService;

@RestController
@RequestMapping("/position")
public class PositionController {

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private PositionService positionService;

    @GetMapping
    public List<Position> findAll() {
        return positionRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Position> findById(@PathVariable Long id) {
        Position positionFound = positionService.findById(id);

        return positionFound != null ? ResponseEntity.ok(positionFound) : ResponseEntity.notFound().build();
    }

    @GetMapping("/search")
    public List<Position> searchByNameAndArea(@RequestParam String name) {
        return positionService.searchByNameAndArea(name);
    }

    @PostMapping
    public ResponseEntity<Position> create(@Valid @RequestBody Position position) {
        Position newPosition = positionService.create(position);

        return ResponseEntity.status(HttpStatus.CREATED).body(newPosition);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Position> update(@PathVariable Long id, @Valid @RequestBody Position position) {
        Position positionFound = positionService.update(id, position);

        return ResponseEntity.ok(positionFound);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        positionService.deleteById(id);
    }

}
