package com.tcs.londrina.codel.vagas.service;

import com.tcs.londrina.codel.vagas.exception.experience.ExperienceInUseException;
import com.tcs.londrina.codel.vagas.exception.experience.ExperienceNotFoundException;
import com.tcs.londrina.codel.vagas.model.Experience;
import com.tcs.londrina.codel.vagas.repository.ExperienceRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class ExperienceService {

    @Autowired
    private ExperienceRepository experienceRepository;

    public Experience findById(Long id) {
        return experienceRepository.findById(id).orElseThrow(() -> new ExperienceNotFoundException(id));
    }

    public Experience create(Experience experience) {
        return experienceRepository.save(experience);
    }

    public Experience update(Long id, Experience experience) {
        Experience experienceFound = findById(id);

        BeanUtils.copyProperties(experience, experienceFound, "id");

        return experienceRepository.save(experienceFound);
    }

    public void deleteById(Long id) {
        try {
            experienceRepository.deleteById(id);

        } catch (EmptyResultDataAccessException e) {
            throw new ExperienceNotFoundException(id);

        } catch (DataIntegrityViolationException e) {
            throw new ExperienceInUseException(id);
        }
    }

}
