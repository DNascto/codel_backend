package com.tcs.londrina.codel.vagas.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "experience")
@JsonIgnoreProperties({ "id_candidate", "id_position" })
public class Experience {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    private LocalDate startDate;

    private LocalDate endDate;

    private String company;

    private String roleTitle;

    private String roleDescription;

    @ManyToOne
    @JoinColumn(name = "id_candidate")
    @JsonProperty("id_candidate")
    private Candidate candidate;

    @ManyToOne
    @JoinColumn(name = "id_position")
    @JsonProperty("id_position")
    private Position position;

}
