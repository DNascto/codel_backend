package com.tcs.londrina.codel.vagas.exception.experience;

public class DuplicateExperienceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DuplicateExperienceException(String message) {
        super(message);
    }

    public DuplicateExperienceException(Long id) {
        this(String.format("Experiência já cadastrada com o id %d", id));
    }

}